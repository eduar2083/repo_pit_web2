import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { InfoComponent } from './pages/info/info.component';
import { UsuarioComponent } from './pages/usuario/usuario.component';
import { NabvarComponent } from './pages/shared/nabvar/nabvar.component';
import { CasosActivosComponent } from './pages/casos-activos/casos-activos.component';
import { RegistrarCasosComponent } from './pages/registrar-casos/registrar-casos.component';

@NgModule({
  declarations: [
    AppComponent,
    InfoComponent,
    UsuarioComponent,
    NabvarComponent,
    CasosActivosComponent,
    RegistrarCasosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
