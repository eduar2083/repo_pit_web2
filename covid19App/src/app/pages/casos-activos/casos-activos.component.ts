import { Component, OnInit } from '@angular/core';
import { UbigeoService } from 'src/app/services/ubigeo.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-casos-activos',
  templateUrl: './casos-activos.component.html',
  styleUrls: ['./casos-activos.component.css']
})
export class CasosActivosComponent implements OnInit {

  forma: FormGroup;
  cargando = true;
  estados: any[] = [];
  casos: any[] = [];

  // Div de casos activos
  partFrom: any = [{
    div    : 0,
    activo : false,
    title  : 'Triaje'
  }, {
    div    : 1,
    activo : true,
    title  : 'Evaluados'
  }, {
    div    : 2,
    activo : true,
    title  : 'Procesados'
  }];

  // Busqueda
  buscar: any[] = [{
    Nombre: '',
    NroDocumentoIdentidad: '',
    EstadoId: 0
  }];

  //



  constructor( private ubigeoService: UbigeoService,
               private fb: FormBuilder ) {
    this.crearFormulario();
    this.cargarDataAlFormulario();
    this.cargarCombo();
  }

  ngOnInit(): void {
    console.log();

  }

  cargarCombo() {

    this.ubigeoService.getEstados()
      .subscribe( (resp: any) => {
        this.estados = resp.filter( res => res.estadoid <= 2);
        this.estados.unshift({
          estadoid: 0,
          nombre: '[ Seleccione Estado ]',
          descripcion: ''
        });

      });

  }
  cargarCombo1( div: number ) {
    if (this.partFrom[div].div === 0) {
      this.ubigeoService.getEstados()
        .subscribe( (resp: any) => {
          this.estados = resp.filter( res => res.estadoid <= 2);
          this.estados.unshift({
            estadoid: 0,
            nombre: '[ Seleccione Estado ]',
            descripcion: ''
          });

        });

    }

    if (this.partFrom[div].div === 1) {
      this.ubigeoService.getEstados()
      .subscribe( (resp: any) => {
        this.estados = resp.filter( res => res.estadoid >= 3 &&  res.estadoid <= 4);
        this.estados.unshift({
          estadoid: 0,
          nombre: '[ Seleccione Estado ]',
          descripcion: ''
        });

      });
    }

    if (this.partFrom[div].div === 2) {
      this.ubigeoService.getEstados()
      .subscribe( (resp: any) => {
        this.estados = resp.filter( res => res.estadoid >= 5 &&  res.estadoid <= 6);
        this.estados.unshift({
          estadoid: 0,
          nombre: '[ Seleccione Estado ]',
          descripcion: ''
        });

      });
    }


  }

  // Prueba de Navegacion
  pageDiv( div: number) {
    console.log(this.partFrom[div].div);

    this.partFrom[div].activo = false;

    this.partFrom.forEach( ctrl => {
      if (!ctrl.activo) {
        if (ctrl.div !== div) {
          this.partFrom[ctrl.div].activo = true;
          this.cargarCombo1(div);
        }
      }
    });
  }

  crearFormulario() {
    this.forma = this.fb.group({
      Nombre: [''],
      NroDocumentoIdentidad: [''],
      EstadoId: ['']
    });
  }

  cargarDataAlFormulario() {

    // this.forma.setValue({
    this.forma.reset({
      Nombre: '',
      NroDocumentoIdentidad: '',
      EstadoId: 0,
    });

  }

  filtrar() {
    console.log(this.forma);
    console.log(this.forma.value);
    if ( this.forma.invalid ) {
      console.log('Formulario invalido');
      return;
    }
    console.log(this.forma.value.EstadoId);
    // tslint:disable-next-line:radix
    this.forma.value.EstadoId = parseInt( this.forma.value.EstadoId );
    console.log(this.forma.value.EstadoId);
    this.ubigeoService.filtro( this.forma.value ).subscribe( (resp: any) => {

      this.casos = resp;
      console.log(this.casos);
      this.cargando = false;
    });
  }


}
